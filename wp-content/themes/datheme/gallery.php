<?php
/**
 * Template Name: Gallery
 *
 */

get_header();
?>

	<!-- BEGIN CONTENT -->
	<section id="content">
		<div class="wrapper page_text">
			<h1 class="page_title"><?php the_title();?></h1>
			<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>

			<div class="page_gallery">
				<div class="columns">
					<?php
					$gallery_posts = new WP_Query();
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					$args =  array(
						'posts_per_page' => 3,
						'orderby'     => 'date',
						'order'       => 'DESC',
						'post_type'   => 'gallery',
						'paged'       => $paged,
					);

					$gallery_posts->query( $args );

					if ( $gallery_posts->have_posts() ) {
						while ( $gallery_posts->have_posts() ) {
							$gallery_posts->the_post();
						$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array(446, 294) );
						?>
							<div class="column column50">
								<div class="image">
									<img src="<?php echo $image_attributes[0];?>" width="<?php echo $image_attributes[1] ?>" height="<?php echo $image_attributes[2]?>" alt="" />
									<p class="caption">
										<strong><?php the_title();?></strong>
										<span><?php the_excerpt();?></span>
										<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'small' );?>" data-rel="prettyPhoto[gallery]" class="button button_small button_orange float_right lightbox">
											<span class="inside"><?php _e( 'zoom', 'datheme' )?></span>
										</a>
									</p>
								</div>
							</div>
						<?php
						}
					}
                    the_posts_pagination();
					wp_reset_postdata();
					?>

				</div>
			</div>

            <ul class="pagenav">
	            <?php
	            echo paginate_links( array(
		            'format'    => '?paged=%#%',
		            'current'   => max( 1, get_query_var('paged') ),
		            'total'     => $gallery_posts->max_num_pages,
		            'prev_text' => '<<',
		            'next_text' => '>>',
                    'type'      => 'plain',
	            ) );
	            ?>
            </ul>

        </div>
	</section>
	<!-- END CONTENT -->

<?php get_footer();
