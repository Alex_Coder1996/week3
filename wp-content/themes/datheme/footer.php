<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 */

?>
<div id="page_bottom">
    <!-- BEGIN ABOVE_FOOTER -->
    <section id="above_footer">
        <div class="wrapper above_footer_boxes page_text">
            <div class="box first">
                <h3><?php _e( 'About us', 'datheme' )?></h3>
                <p><?php the_field( 'about_us', get_option( 'page_on_front' ) );?></p>
            </div>

            <div class="box second">
                <h3><?php _e( 'Recent Posts', 'datheme' )?></h3>
                <ul class="recent_posts">
	                <?php
	                $lastposts = get_posts( [
		                'posts_per_page' => 3,
	                ] );

	                foreach( $lastposts as $post ){
		                setup_postdata($post);
		                ?>
                            <li class="item">
                                <a class="thumbnail" href="#"><img alt="" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );?>"></a>
                                <div class="text">
                                    <h4 class="title"><a href="<?php the_permalink();?>"><?php echo get_the_excerpt();?></a></h4>
                                    <p class="data">
                                        <span class="date"><?php echo get_the_date();?></span>
                                    </p>
                                </div>
                            </li>
		                <?php
	                }
	                wp_reset_postdata();
	                ?>

                </ul>
            </div>

            <div class="box third">
                <h3><?php _e( 'Categories', 'datheme' )?></h3>
                <ul class="menu categories page_text">
                    <?php
                    $da_categories = get_categories(array(
                        'orderby' => 'name',
                        'order' => 'ASC'
                    ));
                    foreach( $da_categories as $category ){
                        echo '<li><a href="' . get_category_link( $category->term_id ) . '">' . $category->name . '('. $category->count . ')</a></li>';
                    }
                    ?>
                </ul>
            </div>

            <div class="box fourth">
                <h3><?php _e( 'Contact Us', 'datheme' );?></h3>
                <ul class="list_contact page_text">
	                <?php
                    $id_front_page = get_option( 'page_on_front' );
	                if ( have_rows( 'contact_us', $id_front_page ) ) {
		                while ( have_rows('contact_us', $id_front_page ) ) { the_row();
			                ?>
                            <li class="phone"><?php the_sub_field( 'phone_number', $id_front_page );?></li>
                            <li class="email"><a href="#"><?php the_sub_field( 'email', $id_front_page );?></a></li>
                            <li class="address"><?php the_sub_field( 'address', $id_front_page );?></li>
			                <?php
		                }
	                }
	                ?>
                </ul>
            </div>
        </div>
    </section>
    <!-- END ABOVE_FOOTER -->

    <!-- BEGIN FOOTER -->
    <footer id="footer">
        <div class="wrapper">
            <?php the_field( 'copyright', get_option( 'page_on_front' ) );?>
            <a href="#page" class="up">
                <span class="arrow"></span>
                <span class="text"><?php _e( 'top', 'datheme' );?>></span>
            </a>
        </div>
    </footer>
    <!-- END FOOTER -->
</div>
</div>
<?php wp_footer();?>
</body>
</html>
