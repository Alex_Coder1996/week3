<?php
/**
 * The template for displaying the single portfolio post
 */

get_header();?>

	<!-- BEGIN CONTENT -->
	<section id="content">
		<div class="wrapper page_text">
			<h1 class="page_title"><?php the_title();?></h1>
			<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>

			<div class="columns">
				<div class="column column33">
                <?php while ( have_posts() ) :
                    the_post();
                ?>
					<h1><?php the_title();?></h1>
					<p><?php
						the_content();
						?></p>
					<h1><?php the_author();?></h1>
				</div>

				<div class="column column66">
                    <div id="content_slide">
                        <div class="flexslider">
                            <a
                                href="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'full' );?>"
                                class="lightbox" data-rel="prettyPhoto[gallery]">
                                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'full' );?>" alt="1" />
                            </a>
                        </div>
                    </div>
				</div>
                <?php endwhile;?>
			</div>
		</div>
	</section>
	<!-- END CONTENT -->


<?php get_footer();
