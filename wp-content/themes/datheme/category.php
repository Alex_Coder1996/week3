<?php
/**
 * The template for displaying Category pages
 *
 */

get_header(); ?>

    <div id="container">
        <div id="content" role="main">

            <h1 class="page-title"><?php
				printf( __( 'Category Archives: %s', 'datheme' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
			<?php
			$category_description = category_description();
			if ( ! empty( $category_description ) )
				echo '<div class="archive-meta">' . $category_description . '</div>';
			get_template_part( 'loop', 'category' );


            $my_posts = get_posts( array(
            'numberposts' => -1,
            'category_name'    => single_cat_title( '', false ),
            'orderby'     => 'date',
            'post_type'   => 'portfolio',
            ) );
            ?>
            <ul>
                <?php
            foreach( $my_posts as $post ){
            setup_postdata( $post );?>
                <li data-type="<?php echo implode(" ", wp_get_post_categories( $post->ID, array('fields' => 'names') ) );?>" data-id="<?php echo $portfolio_post_index;?>" class="column column33">
                                <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'small' );?>" data-rel="prettyPhoto[gallery]" class="portfolio_image lightbox">
                                    <div class="inside">
                                        <img alt="" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail' )[0];?>">
                                        <div class="mask"></div>
                                    </div>
                                </a>

                                <h1><?php the_title();?></h1>
                                <?php the_excerpt();?>
                                <a class="button button_small button_orange" href="<?php the_permalink(); ?>">
                                    <span class="inside"><?php _e( 'read more', 'datheme' )?></span>
                                </a>
                            </li>
                            <?php
            }?>
            </ul>
            <?php

            wp_reset_postdata();?>

        </div><!-- #content -->
    </div><!-- #container -->

<?php
get_footer();