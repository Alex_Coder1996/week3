<?php
/**
 * Template Name: Home page
 *
 */

get_header(); ?>

<!-- BEGIN TOP -->
<section id="top">
	<div class="wrapper">
		<div id="top_slide" class="flexslider">
			<ul class="slides">
                <?php
                if ( have_rows( 'slider' ) ) { // если найдены данные
	                while ( have_rows('slider' ) ) { the_row(); // цикл по строкам данных
                ?>
                    <li>
                        <img src="<?php the_sub_field( 'slide' );?>" alt="" />
                        <p class="flex-caption">
                            <strong><?php the_sub_field( 'title' );?></strong>
                            <span><?php the_sub_field( 'desc' );?></span>
                        </p>
                    </li>
                <?php
	                }
                }
                ?>
			</ul>
		</div>
	</div>
</section>
<!-- END TOP -->

<!-- BEGIN CONTENT -->
<section id="content">
	<div class="wrapper page_text page_home">
		<div class="introduction">
			<?php the_field('introduction');?>
			<a class="button button_big button_orange float_left"><span class="inside"><?php _e('read more', 'datheme');?></span></a>
		</div>

		<ul class="columns dropcap">
            <?php
                if ( have_rows( 'dropcap' ) ) {
	                while ( have_rows('dropcap' ) ) { the_row();
            ?>
			<li class="column column33 first">
				<div class="inside">
					<h1><?php the_sub_field( 'dropcap_title' );?></h1>
					<p><?php the_sub_field( 'dropcap_desc' );?></p>
					<p class="read_more"><a href="<?php the_sub_field( 'dropcap_link' );?>"><?php _e('Read more', 'datheme');?></a></p>
				</div>
			</li>
            <?php
                    }
                }
            ?>
		</ul>

		<ul class="columns iconcap">
			<?php
			if ( have_rows( 'iconcap' ) ) {
				while ( have_rows('iconcap' ) ) { the_row();
					?>
                    <li class="column column33 first">
                        <div class="inside">
                            <h1><?php the_sub_field( 'iconcap_title' );?></h1>
                            <p><?php the_sub_field( 'iconcap_desc' );?></p>
                            <p class="read_more"><a href="<?php the_sub_field( 'iconcap_link' );?>"><?php _e('Read more', 'datheme');?></a></p>
                        </div>
                    </li>
					<?php
				}
			}
			?>
		</ul>

		<div class="underline"></div>

		<div class="portfolio">
			<p class="all_projects"><a href="#"><?php _e('View all projects', 'datheme');?></a></p>
			<h1><?php _e('Portfolio', 'datheme');?></h1>
			<div class="columns">

				<?php
				if ( have_rows( 'portfolio' ) ) {
					while ( have_rows('portfolio' ) ) { the_row();
						?>
                        <div class="column column25">
                            <a href="<?php the_sub_field( 'big_photo' );?>" class="image lightbox" data-rel="prettyPhoto[gallery]">
								<span class="inside">
									<img src="<?php the_sub_field( 'small_photo' );?>" alt="" />
									<span class="caption"><?php the_sub_field( 'photo_title' );?></span>
								</span>
                                <span class="image_shadow"></span>
                            </a>
                        </div>
						<?php
					}
				}
				?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</section>
<!-- END CONTENT -->
</div>
</div>
<?php get_footer(); ?>


