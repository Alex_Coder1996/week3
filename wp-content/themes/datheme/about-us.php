<?php
/**
 * Template Name: Static page
 *
 */

get_header();
?>

    <!-- BEGIN CONTENT -->
    <section id="content">
        <div class="wrapper page_text">
            <h1 class="page_title"><?php the_title();?></h1>
                <section id="content">
                    <div class="about">
                        <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
                        <?php if ( have_posts() ) :
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile;
                        endif; ?>
                    </div>
                </section>

            </div>
        </div>
    </section>
    <!-- END CONTENT -->

<?php
get_footer();