<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>

<h2><?php esc_html_e( 'Error 404 - Page Not Found', 'datheme' ); ?></h2>

<?php get_footer(); ?>
