<?php
/*
 * Template name: Portfolio
 */

get_header();?>

    <section id="content">
        <div class="wrapper page_text">
            <h1 class="page_title"><?php post_type_archive_title();?></h1>
	        <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>

            <ul id="portfolio_categories" class="portfolio_categories">
	            <?php
	            echo '<li class="active segment-0"><a href="" class="all">' . __( 'All Categories', 'datheme' ) . ' </a></li>';
                $segment_index = 0;
	            $da_categories = get_categories(array(
		            'orderby' => 'name',
		            'order' => 'ASC'
	            ));
	            foreach( $da_categories as $category ){
                    $segment_index++;
                    echo '<li class="segment-'. $segment_index . '"><a href="' . get_category_link( $category->term_id ) . '" class="' . $category->name . '">'. $category->name . '</a></li>';
	            }
	            ?>
            </ul>

            <div class="portfolio_items_container">
                <ul class="portfolio_items columns">

                    <?php
                        $portfolio_posts = get_posts( array(
                            'numberposts' => -1,
                            'orderby'     => 'date',
                            'order'       => 'DESC',
                            'post_type'   => 'portfolio',
                        ) );

                        $portfolio_post_index = 0;
                        foreach( $portfolio_posts as $post ){
                            setup_postdata( $post );
	                        $portfolio_post_index++;
                    ?>
                            <li data-type="<?php echo implode(" ", wp_get_post_categories( $post->ID, array('fields' => 'names') ) );?>" data-id="<?php echo $portfolio_post_index;?>" class="column column33">
                                <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'small' );?>" data-rel="prettyPhoto[gallery]" class="portfolio_image lightbox">
                                    <div class="inside">
                                        <img alt="" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'thumbnail' )[0];?>">
                                        <div class="mask"></div>
                                    </div>
                                </a>

                                <h1><?php the_title();?></h1>
                                <?php the_excerpt();?>
                                <a class="button button_small button_orange" href="<?php the_permalink(); ?>">
                                    <span class="inside"><?php _e( 'read more', 'datheme' )?></span>
                                </a>
                            </li>
	                        <?php
                        }
                        wp_reset_postdata();
                            ?>

                </ul>
            </div>
        </div>
    </section>
    <!-- END CONTENT -->

<?php get_footer();
