<?php
/*
 * Template name: Blog
 */

get_header();
?>

	<!-- BEGIN CONTENT -->
	<section id="content">
		<div class="wrapper page_text">
			<h1 class="page_title"><?php the_title();?></h1>

			<?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>

			<div class="columns">
				<div class="column column75">

					<?php
					$args = array(
						'posts_per_page' => 3,
						'post_type'      => 'post',
					);
					$query = new WP_Query( $args );

					if ( $query->have_posts() ) : ?>

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <article class="article">
                                <div class="article_image nomargin">
                                    <div class="inside">
                                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'full' );?>" alt="" />
                                    </div>
                                </div>
                                <div class="article_details">
                                    <ul class="article_author_date">
                                        <li><em><?php _e( 'Add:', 'datheme' )?></em> <?php echo get_the_date();?></li>
                                        <li><em><?php _e( 'Author:', 'datheme' )?></em> <a href="#"><?php the_author();?></a></li>
                                    </ul>
                                    <p class="article_comments"><em><?php _e( 'Comment:', 'datheme' )?></em> 142</p>
                                </div>

                                <h1><?php the_title();?></h1>
                                <p><?php the_content();?></p>
                                <a class="button button_small button_orange float_left" href="<?php the_permalink();?>">
                                    <span class="inside"><?php _e( 'read more', 'datheme' )?></span>
                                </a>
                            </article>
						<?php endwhile;

						wp_reset_postdata(); ?>

					<?php else : ?>
                        <p><?php _e( 'No posts' ); ?></p>
					<?php endif; ?>
                </div>





				</div>

				<div class="column column25">
					<div class="padd16bot">
						<h1>Search</h1>
						<form class="searchbar">
							<fieldset>
								<div>
									<span class="input_text"><input type="text" class="clearinput" value="Search..." /></span>
									<button type="button" class="input_submit"><span>Search</span></button>
								</div>
							</fieldset>
						</form>
					</div>

					<div class="padd16bot">
						<h1>Recent Posts</h1>
						<ul class="recent_posts">
							<li class="item">
								<a class="thumbnail" href="#"><img alt="" src="./gfx/examples/above_footer_recent_posts1.jpg"></a>
								<div class="text">
									<h4 class="title"><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></h4>
									<p class="data">
										<span class="date">21/10/2011</span>
									</p>
								</div>
							</li>
							<li class="item">
								<a class="thumbnail" href="#"><img alt="" src="./gfx/examples/above_footer_recent_posts2.jpg"></a>
								<div class="text">
									<h4 class="title"><a href="#">Cras vitae est lacus vehicula enim ac turpis at tellus.</a></h4>
									<p class="data">
										<span class="date">21/10/2011</span>
									</p>
								</div>
							</li>
							<li class="item">
								<a class="thumbnail" href="#"><img alt="" src="./gfx/examples/above_footer_recent_posts3.jpg"></a>
								<div class="text">
									<h4 class="title"><a href="#">Quisque quis nibh.</a></h4>
									<p class="data">
										<span class="date">21/10/2011</span>
									</p>
								</div>
							</li>
						</ul>
					</div>

					<div class="padd16bot">
						<h1>About Us</h1>
						<p>Suspendisse in faucibus lorem, pretium quis, <a href="#">lacinia aliquet</a> enim sapien et lacus tellus quis consectetuer nisl.</p>
						<p>Vestibulum tempus. Pellentesque sagittis, nunc eu odio. Suspendisse turpis at ipsum. Pellentesque placerat. Vivamus vulputate luctus.</p>
					</div>

					<div class="padd16bot">
						<h1>Categories</h1>
						<ul class="menu categories page_text">
							<li><a href="#">Webdesign (8)</a></li>
							<li>
								<a href="#">Branding (12)</a>
								<ul>
									<li><a href="#">Photography (45)</a></li>
								</ul>
							</li>
							<li><a href="#">Photomanipulation (5)</a></li>
							<li><a href="#">3D (1)</a></li>
							<li><a href="#">Others (7)</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END CONTENT -->

<?php get_footer();
