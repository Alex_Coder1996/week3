<?php
/**
 * The template for single post page
 */

get_header();?>

	<!-- BEGIN CONTENT -->
	<section id="content">
	<div class="wrapper page_text">
		<h1 class="page_title"><?php the_title();?></h1>
			<?php
			while ( have_posts() ) : the_post();
			the_content();?>
			<article class="article">
                <div class="article_image nomargin">
                    <div class="inside">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(), 'full' );?>" alt="" />
                    </div>
                </div>
                <div class="article_details">
                    <ul class="article_author_date">
                        <li><em><?php _e( 'Add:', 'datheme' )?></em> <?php echo get_the_date();?></li>
                        <li><em><?php _e( 'Author:', 'datheme' )?></em> <a href="#"><?php the_author();?></a></li>
                    </ul>
                    <p class="article_comments"><em><?php _e( 'Comment:', 'datheme' )?></em><?php echo get_comments_number();?></p>
                </div>
			</article>
			<?php
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'datheme' ) . '</span> ' .
					               '<span class="screen-reader-text">' . __( 'Next post:', 'datheme' ) . '</span> ' .
					               '<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'datheme' ) . '</span> ' .
					               '<span class="screen-reader-text">' . __( 'Previous post:', 'datheme' ) . '</span> ' .
					               '<span class="post-title">%title</span>',
				) );

			endwhile;
			?>

			</div>
		</div>
	</section>
	<!-- END CONTENT -->

<?php get_footer();
