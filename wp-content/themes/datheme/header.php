<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
</head>
<?php wp_head();?>
<body>

<!-- BEGIN STYLESHEET SWITCHER -->
<div id="stylesheet_switcher">
    <a href="#" id="switcher"></a>
    <ul id="stylesheets">
        <li>
            <a href="#" class="sheet" id="light">
                <span class="image"><img src="<?php echo the_field( 'switch_light_img', get_option( 'page_on_front' ) );?>" alt="" /></span>
                <span class="mask"></span>
                <span class="name"><?php _e( 'Light version', 'datheme' )?></span>
            </a>
        </li>
        <li>
            <a href="#" class="sheet" id="dark">
                <span class="image"><img src="<?php echo the_field( 'switch_dark_img', get_option( 'page_on_front' ) );?>" alt="" /></span>
                <span class="mask"></span>
                <span class="name"><?php _e( 'Dark version', 'datheme' )?></span>
            </a>
        </li>
    </ul>
</div>
<!-- END STYLESHEET SWITCHER -->

<!-- BEGIN PAGE -->
<div id="page">
    <div id="page_top">
        <div id="page_top_in">
            <!-- BEGIN TITLEBAR -->
            <header id="titlebar">
                <div class="wrapper">
                    <a id="logo" href="<?php get_home_url();?>"><span></span></a>
                    <div id="titlebar_right">
                        <ul id="social_icons">
	                        <?php
	                        $id_front_page = get_option( 'page_on_front' );
	                        if ( have_rows( 'contact_us', $id_front_page ) ) {

		                        while ( have_rows('contact_us', $id_front_page ) ) { the_row();
			                        ?>
                                    <li><a href="<?php get_sub_field( 'linkedin_link', $id_front_page );?>" class="linkedin"></a></li>
                                    <li><a href="<?php get_sub_field( 'facebook_link', $id_front_page );?>" class="facebook"></a></li>
                                    <li><a href="<?php get_sub_field( 'twitter_link', $id_front_page );?>" class="twitter"></a></li>
                                    <li><a href="<?php get_sub_field( 'rss_link', $id_front_page );?>" class="rss"></a></li>
			                        <?php
		                        }
	                        }
	                        ?>
                        </ul>
                        <div class="clear"></div>

                        <?php
                        wp_nav_menu( array(
	                        'menu'            => 'Main menu',
	                        'container'       => 'nav',
	                        'container_class' => '',
	                        'container_id'    => '',
	                        'menu_class'      => '',
	                        'menu_id'         => 'top_menu',
	                        'echo'            => true,
	                        'depth'           => 2,
                        ) );
                        ?>

                    </div>
                    <div class="clear"></div>
                </div>
            </header>
            <!-- END TITLEBAR -->
